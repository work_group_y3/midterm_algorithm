import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testA1_7 {

	@Test
	void test1() {
		A1_7 a1_7 = new A1_7();

		int[] array = { 2, 2, 3, 8, 4 };
		boolean result = a1_7.CheckPairnum(array, 11); // get array, c

		assertEquals(true, result); // check boolean result == true
	}

	@Test
	void test2() {
		A1_7 a1_7 = new A1_7();

		int[] array = { 2, 5, 4, 9, 4 };
		boolean result = a1_7.CheckPairnum(array, 8); // get array, c

		assertEquals(true, result); // check boolean result == true
	}

	@Test
	void test3() {
		A1_7 a1_7 = new A1_7();

		int[] array = { 2, 10, 3, 8, 1 };
		boolean result = a1_7.CheckPairnum(array, 13); // get array, c

		assertEquals(true, result); // check boolean result == true
	}

	@Test
	void test4() {
		A1_7 a1_7 = new A1_7();

		int[] array = { 5, 1, 1, 7, 2 };
		boolean result = a1_7.CheckPairnum(array, 2); // get array, c

		assertEquals(true, result); // check boolean result == true
	}

	@Test
	void test5() {
		A1_7 a1_7 = new A1_7();

		int[] array = { 10, 2, 3, 4, 6 };
		boolean result = a1_7.CheckPairnum(array, 9); // get array, c

		assertEquals(true, result); // check boolean result == true
	}

	@Test
	void test6() {
		A1_7 a1_7 = new A1_7();

		int[] array = { 4, 4, 10, 2, 9 };
		boolean result = a1_7.CheckPairnum(array, 5); // get array, c

		assertEquals(true, result); // check boolean result == true
	}

	@Test
	void test7() {
		A1_7 a1_7 = new A1_7();

		int[] array = { 11, 5, 6, 6, 8 };
		boolean result = a1_7.CheckPairnum(array, 7); // get array, c

		assertEquals(true, result); // check boolean result == true
	}

	@Test
	void test8() {
		A1_7 a1_7 = new A1_7();

		int[] array = { 2, 3, 3, 10, 11 };
		boolean result = a1_7.CheckPairnum(array, 8); // get array, c

		assertEquals(false, result); // check boolean result == false
	}

	@Test
	void test9() {
		A1_7 a1_7 = new A1_7();

		int[] array = { 3, 15, 13, 11, 2 };
		boolean result = a1_7.CheckPairnum(array, 10); // get array, c

		assertEquals(false, result); // check boolean result == false
	}

	@Test
	void test10() {
		A1_7 a1_7 = new A1_7();

		int[] array = { 2, 1, 8, 8, 9 };
		boolean result = a1_7.CheckPairnum(array, 6); // get array, c

		assertEquals(false, result); // check boolean result == false
	}

}