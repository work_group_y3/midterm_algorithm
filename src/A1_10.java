import java.util.Scanner;

public class A1_10 {

	public static int checkSubArr(int A[]) {

		int sub = 1, len = 1, subindex = 0;

		for (int i = 1; i < A.length; i++) {

			if (A[i] > A[i - 1])
				len++;
			else {
				if (sub < len) {
					sub = len;
					subindex = i - sub;
				}
				len = 1;
			}
		}
		if (sub < len) {
			sub = len;
			return sub;
		}
		return 0;
	}

	public static void LenofSubArr(int A[]) {

		int sub = 1, len = 1, subindex = 0;

		for (int i = 1; i < A.length; i++) {

			if (A[i] > A[i - 1])
				len++;
			else {
				if (sub < len) {
					sub = len;
					subindex = i - sub;
				}
				len = 1;
			}
		}
		if (sub < len) {
			sub = len;
			subindex = A.length - sub;

		}

		System.out.println("Length: " + sub);
		System.out.print("The subarray is ");

		for (int i = subindex; i < sub + subindex; i++) {
			System.out.print(A[i] + " ");
		}

	}

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);

		int n;
		System.out.print("Integer n: ");
		n = kb.nextInt();

		int[] A = new int[n];
		System.out.print("Get array A: ");
		for (int i = 0; i < A.length; i++) {
			A[i] = kb.nextInt();
		}

		LenofSubArr(A);

	}

}
