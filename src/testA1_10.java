import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testA1_10 {

	@Test
	void test1() {
		A1_10 a1_10 = new A1_10();

		int[] array = { 1, 2, 2, 3, 4 };

		assertEquals(3, a1_10.checkSubArr(array)); // check boolean result == true
	}
	
	@Test
	void test2() {
		A1_10 a1_10 = new A1_10();

		int[] array = { 5, 4, 4, 8, 9 };

		assertEquals(3, a1_10.checkSubArr(array)); // check boolean result == true
	}

	@Test
	void test3() {
		A1_10 a1_10 = new A1_10();

		int[] array = { 8, 4, 2, 3, 5, 6 };

		assertEquals(4, a1_10.checkSubArr(array)); // check boolean result == true
	}

	@Test
	void test4() {
		A1_10 a1_10 = new A1_10();

		int[] array = { 3, 1, 8, 5, 6, 10 };

		assertEquals(3, a1_10.checkSubArr(array)); // check boolean result == true
	}

	@Test
	void test5() {
		A1_10 a1_10 = new A1_10();

		int[] array = { 9, 5, 5, 2, 3, 6 };

		assertEquals(3, a1_10.checkSubArr(array)); // check boolean result == true
	}
	
	@Test
	void test6() {
		A1_10 a1_10 = new A1_10();

		int[] array = { 1, 3, 5, 10, 2, 8 };

		assertEquals(6, a1_10.checkSubArr(array)); // check boolean result == true
	}

	@Test
	void test7() {
		A1_10 a1_10 = new A1_10();

		int[] array = { 5, 10, 12, 1, 6, 8, 10};

		assertEquals(2, a1_10.checkSubArr(array)); // check boolean result == true
	}
	
	@Test
	void test8() {
		A1_10 a1_10 = new A1_10();

		int[] array = { 2, 1, 5, 3, 1};

		assertEquals(4, a1_10.checkSubArr(array)); // check boolean result == true
	}
	
	@Test
	void test9() {
		A1_10 a1_10 = new A1_10();

		int[] array = { 9, 10, 11, 2, 5, 4 };

		assertEquals(2, a1_10.checkSubArr(array)); // check boolean result == true
	}
	
	@Test
	void test10() {
		A1_10 a1_10 = new A1_10();

		int[] array = { 1, 5, 5, 2, 3, 6 };

		assertEquals(4, a1_10.checkSubArr(array)); // check boolean result == true
	}

}
