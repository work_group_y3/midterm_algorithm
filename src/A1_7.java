import java.util.Scanner;

public class A1_7 {

	static boolean CheckPairnum(int A[], int c) {
		for (int i = 0; i < (A.length - 1); i++) {
			for (int j = (i + 1); j < A.length; j++) {
				if (A[i] + A[j] == c) {
					System.out.println("Given sum " + c + " is (" + A[i] + ", " + A[j] + ")");

					return true;
				}
			}
		}

		return false;
	}

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);

		int n, c;
		System.out.print("Integer n: ");
		n = kb.nextInt();
		System.out.print("Integer c: ");
		c = kb.nextInt();

		int[] A = new int[n];
		System.out.print("Array A: ");
		for (int i = 0; i < A.length; i++) {
			A[i] = kb.nextInt();
		}

		if (!CheckPairnum(A, c))
			System.out.println("No sum for " + c);

	}

}
